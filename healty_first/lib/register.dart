import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:healty_first/LoginScreen.dart';
import 'package:healty_first/main.dart';

Future<void> main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class register extends StatefulWidget {
  @override
  _registerState createState() => _registerState();
}

class _registerState extends State<register> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.all(0),
        child: ListView(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 110),
                Text(
                  "Register",
                  style: TextStyle(fontSize: 36),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(30),
              child: TextField(
                controller: _emailController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    labelText: "E-mail"),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
              child: TextField(
                obscureText: true,
                controller: _passwordController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    labelText: "Password"),
              ),
            ),
            SizedBox(height: 60),
            Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: ElevatedButton(
                child: Text("Register"),
                onPressed: () async {
                  await _firebaseAuth.createUserWithEmailAndPassword(
                      email: _emailController.text,
                      password: _passwordController.text);
                },
              ),
            ),
            SizedBox(height: 20),
            Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginScreen()));
                  },
                  child: Text("Login")),
            )
          ],
        ),
      ),
    );
  }
}
