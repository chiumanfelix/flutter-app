import 'dart:convert';

class User {
  final String name, positif, sembuh, meninggal, dirawat;
  User(this.name, this.positif, this.sembuh, this.meninggal, this.dirawat);
}

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));
String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.name,
    this.positif,
    this.sembuh,
    this.meninggal,
    this.dirawat,
  });
  String name;
  String positif;
  String sembuh;
  String meninggal;
  DateTime dirawat;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
      name: json["name"],
      positif: json["positif"],
      sembuh: json["sembuh"],
      meninggal: json["meninggal"],
      dirawat: json["dirawat"]);

  Map<String, dynamic> toJson() => {
        "Name": name,
        "Positif": positif,
        "Sembuh": sembuh,
        "Meninggal": meninggal,
        "dirawat": dirawat,
      };
}
