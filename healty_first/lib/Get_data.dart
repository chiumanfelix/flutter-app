import 'dart:convert';
import 'package:healty_first/DrawerScreen.dart';

import 'Models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class GetDataApi extends StatefulWidget {
  @override
  _GetDataApiState createState() => _GetDataApiState();
}

class _GetDataApiState extends State<GetDataApi> {
  get id => null;
  getUserData() async {
    var response =
        await http.get(Uri.parse("https://api.kawalcorona.com/indonesia/"));
    var jsonData = jsonDecode(response.body);
    List<User> users = [];

    for (var u in jsonData) {
      User user = User(
          u["name"], u["positif"], u["sembuh"], u["meninggal"], u["dirawat"]);
      users.add(user);
    }
    print(users.length);
    return users;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getUserData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Corona Update",
          style: TextStyle(color: Colors.black),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
          )
        ],
      ),
      drawer: DrawerScreen(),
      body: Container(
        child: FutureBuilder(
          future: getUserData(),
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return Container(
                  child: Center(
                child: Text("Loading"),
              ));
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, i) {
                    return Column(
                      children: [
                        SizedBox(height: 30),
                        Text(snapshot.data[i].name,
                            style: TextStyle(fontSize: 32, color: Colors.blue)),
                        SizedBox(height: 30),
                        Text("Positive",
                            style: TextStyle(
                                fontSize: 24, color: Colors.orange[700])),
                        SizedBox(height: 5),
                        Text(snapshot.data[i].positif,
                            style: TextStyle(
                                fontSize: 24, color: Colors.orange[700])),
                        SizedBox(height: 30),
                        Text("Cured",
                            style:
                                TextStyle(fontSize: 24, color: Colors.green)),
                        SizedBox(height: 5),
                        Text(snapshot.data[i].sembuh,
                            style:
                                TextStyle(fontSize: 24, color: Colors.green)),
                        SizedBox(height: 30),
                        Text("Death",
                            style: TextStyle(fontSize: 24, color: Colors.red)),
                        SizedBox(height: 5),
                        Text(snapshot.data[i].meninggal,
                            style: TextStyle(fontSize: 24, color: Colors.red)),
                        SizedBox(height: 30),
                        Text("Treated",
                            style:
                                TextStyle(fontSize: 24, color: Colors.orange)),
                        SizedBox(height: 5),
                        Text(snapshot.data[i].dirawat,
                            style:
                                TextStyle(fontSize: 24, color: Colors.orange)),
                        SizedBox(height: 70),
                        Image.asset("assets/img/7774.jpg")
                      ],
                    );
                  });
            }
          },
        ),
      ),
    );
  }
}
