import 'package:flutter/material.dart';
import 'package:healty_first/DrawerScreen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "News",
          style: TextStyle(color: Colors.black),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
          )
        ],
      ),
      drawer: DrawerScreen(),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search, size: 18),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                  hintText: "Search News"),
            ),
            SizedBox(height: 10),
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Text(
                "Headline",
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue),
              )
            ]),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Image.asset("assets/img/india.jpg")],
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                TextButton(
                    onPressed: () {},
                    child: Text(
                      "India: Why some states do better than others in \ntackling COVID",
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    ),
                    style:
                        ElevatedButton.styleFrom(primary: Colors.transparent)),
                Padding(padding: EdgeInsets.all(10))
              ],
            ),
            Text(
              "India has been struggling to control a devastating second coronavirus wave. But government action against COVID-19 is not uniform, with some states and cities getting a better grip over the health crisis than others.",
              style: TextStyle(fontSize: 16),
            ),
            SizedBox(height: 10),
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Text(
                "See Also",
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue),
              )
            ]),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                TextButton(
                    onPressed: () {},
                    child: Text(
                      "Coronavirus digest: WHO says Africa urgently...",
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    ),
                    style:
                        ElevatedButton.styleFrom(primary: Colors.transparent)),
                Padding(padding: EdgeInsets.all(10))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
