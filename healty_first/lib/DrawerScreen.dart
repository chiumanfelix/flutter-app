import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:healty_first/About.dart';
import 'package:healty_first/Get_data.dart';
import 'package:healty_first/HomeScreen.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser.email);
    }
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Hi There,"),
            currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage("assets/img/doge.jpg")),
            accountEmail: Text(auth.currentUser.email),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "News",
            onTilePressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()));
            },
          ),
          DrawerListTile(
            iconData: Icons.book,
            title: "Corona Update",
            onTilePressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => GetDataApi()));
            },
          ),
          DrawerListTile(
            iconData: Icons.info,
            title: "About/Log Out",
            onTilePressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => About()));
            },
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
